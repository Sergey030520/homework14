#include <iostream>

int main() {
	std::string word = "hello";
	std::cout << "Word: " << word << std::endl;
	std::cout << "Length: " << word.length() << std::endl;
	std::cout << "First symbol: " << word.front() << std::endl;
	std::cout << "Last symbol: " << word.back() << std::endl;
}